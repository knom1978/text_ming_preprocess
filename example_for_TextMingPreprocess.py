#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
from TMP import TextMingPreprocess as tmp

'''
TextMingPreprocess class可以處理pandas dataframe或是字串.
此外也可以利用連鎖寫法來
以下是中英夾雜的文本前處理範例.
'''

#Pandas dataframe
df = pd.read_excel("story_characters.xlsx")
df['Description'] = tmp(df['Description']).ch_en_add_space().all_punctuations_to_space().space_trim().item_generate()
print(df['Description'][0])

#String
input_string = '哈利波特的英文名字是Harry Porter, 他最愛的掃把是光輪2000, It\'s really fast!! 真的?'
output_string = tmp(input_string).ch_en_add_space().all_punctuations_to_space().space_trim().item_generate()
print(output_string)
