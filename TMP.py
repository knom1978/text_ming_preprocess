#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re
import pandas as pd

class TextMingPreprocess(object):
    def __init__(self, item):
        '''傳入字串或dataframe'''
        self._item = item
    def ch_en_add_space(self):
        '''中英文之間插入space'''
        def _ch_en_add_space(matched):
            strValue = str(matched.group())
            addedValue = str(' ' + strValue + ' ')
            return addedValue
        if type(self._item) is pd.core.series.Series:
            self._item = self._item.map(lambda x: re.sub(r'[\u4e00-\u9fa5]{1,}', _ch_en_add_space, x))
        elif type(self._item) == str:
            self._item = re.sub(r'[\u4e00-\u9fa5]{1,}', _ch_en_add_space, self._item)
        print("Space is added between Chinese/English or English/Chinese.")
        return self
    def all_punctuations_to_space(self):
        '''將所有標點符號替換成space'''
        if type(self._item) is pd.core.series.Series:
            self._item = self._item.map(lambda x: re.sub(r'[^a-zA-Z0-9\u4e00-\u9fa5]', ' ', x))
        elif type(self._item) == str:
            self._item = re.sub(r'[^a-zA-Z0-9\u4e00-\u9fa5]', ' ', self._item)
        print('All punctuations are replaced by spaces.')
        return self
    def dash_to_space(self):
        '''將"-"替換成space'''
        if type(self._item) is pd.core.series.Series:
            self._item = self._item.map(lambda x: re.sub(r'(-)', ' ', x))
        elif type(self._item) == str:
            self._item = re.sub(r'(-)', ' ', self._item)
        print("dash is replaced by a space.")
        return self
    def colon_to_space(self):
        '''將":"替換成space'''
        if type(self._item) is pd.core.series.Series:
            self._item = self._item.map(lambda x: re.sub(r'(:)', ' ', x))
        elif type(self._item) == str:
            self._item = re.sub(r'(:)', ' ', self._item)
        print("dash is replaced by a space.")
        return self
    def comma_to_space(self):
        '''將","替換成space'''
        if type(self._item) is pd.core.series.Series:
            self._item = self._item.map(lambda x: re.sub(r'(,)', ' ', x))
        elif type(self._item) == str:
            self._item = re.sub(r'(,)', ' ', self._item)
        print("dash is replaced by a space.")
        return self
    def space_trim(self):
        '''將多個連續space替換成一個space'''
        if type(self._item) is pd.core.series.Series:
            self._item = self._item.map(lambda x: re.sub(r'(\s+)', ' ', x))
        elif type(self._item) == str:
            self._item = re.sub(r'(\s+)', ' ', self._item)
        print("Spaces are removed.")
        return self
    def space_remove(self):
        '''將space全部移除'''
        if type(self._item) is pd.core.series.Series:
            self._item = self._item.map(lambda x: re.sub(r'(\s)', '', x))
        elif type(self._item) == str:
            self._item = re.sub(r'(\s)', '', self._item)
        print("Space is trimmed.")
        return self 
    def item_generate(self):
        '''輸出與原輸入格式相同之輸出'''
        return self._item

