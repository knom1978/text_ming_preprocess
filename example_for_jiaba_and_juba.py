#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import jieba
import juba
import pandas as pd
from TMP import TextMingPreprocess as tmp

#jieba設定
jieba.load_userdict("story_dict.txt")
stopwords =['-', '_', ' ']

#讀excel檔之後使用tmp處理dataframe preprocess
df = pd.read_excel("story_characters.xlsx")
df['Description'] = tmp(df['Description']).ch_en_add_space().all_punctuations_to_space().space_trim().item_generate()
dfToList = df['Description'].tolist()

#利用jiaba斷詞
jlist = [list(jieba.cut(item_text,cut_all=False)) for item_text in dfToList]

#利用juba將斷詞結果收成list
S = juba.Similar(jlist)
vlist = S.vocabularyList
final_list = [x for x in vlist if x not in stopwords]

print(final_list)