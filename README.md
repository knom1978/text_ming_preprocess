處理中英夾雜文本的處理方法
===
###### tags: `text mining` `jieba` `juba`
## Introduction
一般文本不太會有中英夾雜的狀況, 因此要處理這種混雜文本需要特別的前處理.

## Preprocess method
前處理方法是利用regular expression正則表示式切開中英文, 東方文字字符編碼有一定的範圍.

在python之下寫法如下:

```
r'[\u4e00-\u9fa5]{1,}'
```
之後使用map可以施用在整個pandas dataframe的column

```
def _ch_en_add_space(matched):
            strValue = str(matched.group())
            addedValue = str(' ' + strValue + ' ')
            return addedValue
new_dataframe_column = some_dataframe_column.map(lambda x: re.sub(r'[\u4e00-\u9fa5]{1,}', _ch_en_add_space, x))
```
目前已經寫成TMP.py可供使用. 使用方式請見example_for_TextMingPreprocess.py



## jieba and juba method
jieba是處理中文斷詞的套件, 而juba則是可以斷好詞的輸出進一步建成tdm(term-document-matrix)之類的套件.
使用方式請見example_for_jiaba_and_juba.py